package com.centroid.ecommercebuildingmaterials.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.centroid.ecommercebuildingmaterials.Constants.ApiConstants;
import com.centroid.ecommercebuildingmaterials.R;
import com.centroid.ecommercebuildingmaterials.preferencehelper.PreferenceHelper;


public class LoginActivity extends AppCompatActivity {

    Button btnlogin;

    EditText input_email,input_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        btnlogin=findViewById(R.id.btnlogin);
        input_email=findViewById(R.id.input_email);

        input_password=findViewById(R.id.input_password);


        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!input_email.getText().toString().equals(""))
                {

                    if(!input_password.getText().toString().equals(""))
                    {


                        //checkUser();

                        new PreferenceHelper(LoginActivity.this).putData(ApiConstants.USERTOKEN,"1");


                        startActivity(new Intent(LoginActivity.this,HomeActivity.class));
                        finish();


                    }
                    else {

                        Toast.makeText(LoginActivity.this,"Enter the password",Toast.LENGTH_SHORT).show();
                    }


                }
                else {

                    Toast.makeText(LoginActivity.this,"Enter the user name",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }






}
