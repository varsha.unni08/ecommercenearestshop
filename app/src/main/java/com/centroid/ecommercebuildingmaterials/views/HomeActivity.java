package com.centroid.ecommercebuildingmaterials.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.ecommercebuildingmaterials.R;
import com.centroid.ecommercebuildingmaterials.adapters.MaterialTypeAdapter;
import com.centroid.ecommercebuildingmaterials.adapters.MaterialsAdapter;
import com.centroid.ecommercebuildingmaterials.domain.Materials;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    RecyclerView recyclerView_materials;

    TabLayout tablayout;

    String arr[]={"All","Cements","Tiles","Wiring materials","Roofs"};

    int arrimg[]={R.drawable.ic_all,R.drawable.ic_cement,R.drawable.ic_tiles,R.drawable.ic_wire,R.drawable.ic_roof};

    List<Materials>materialsdata=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();
        tablayout=findViewById(R.id.tablayout);
        recyclerView_materials=findViewById(R.id.recyclerad);


        for(int i=0;i<arr.length;i++)
        {
            Materials materials=new Materials();
            materials.setData(arr[i]);
            materials.setSelected(arrimg[i]);
            materialsdata.add(materials);

            tablayout.addTab(tablayout.newTab());

            View view=getLayoutInflater().inflate(R.layout.layout_materialtype,null);

            TextView txtMaterial=view.findViewById(R.id.txtMaterial);

            AppCompatImageView imgtype=view.findViewById(R.id.imgtype);

            txtMaterial.setText(arr[i]);
            imgtype.setImageResource(arrimg[i]);




            tablayout.getTabAt(i).setCustomView(view);

        }

        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                View view=tab.getCustomView();


                CardView card_materials=view.findViewById(R.id.card_materials);

                card_materials.setCardElevation(20);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                View view=tab.getCustomView();
                CardView card_materials=view.findViewById(R.id.card_materials);

                card_materials.setCardElevation(4);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        recyclerView_materials.setLayoutManager(new LinearLayoutManager(HomeActivity.this));
        recyclerView_materials.setAdapter(new MaterialsAdapter(HomeActivity.this));

    }
}
