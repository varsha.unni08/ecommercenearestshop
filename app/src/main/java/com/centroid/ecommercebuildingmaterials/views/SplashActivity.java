package com.centroid.ecommercebuildingmaterials.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.centroid.ecommercebuildingmaterials.Constants.ApiConstants;
import com.centroid.ecommercebuildingmaterials.R;
import com.centroid.ecommercebuildingmaterials.preferencehelper.PreferenceHelper;

public class SplashActivity extends AppCompatActivity {

    Handler handler=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {


                if(new PreferenceHelper(SplashActivity.this).getData(ApiConstants.USERTOKEN).equals("1"))
                {

                    startActivity(new Intent(SplashActivity.this,HomeActivity.class));
                    finish();
                }
                else {


                    startActivity(new Intent(SplashActivity.this,LoginActivity.class));
                    finish();
                }


            }
        },3000);
    }
}
