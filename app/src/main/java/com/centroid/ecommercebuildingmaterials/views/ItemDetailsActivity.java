package com.centroid.ecommercebuildingmaterials.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.ecommercebuildingmaterials.R;
import com.centroid.ecommercebuildingmaterials.domain.Materials;

public class ItemDetailsActivity extends AppCompatActivity {

    Materials materials;

    ImageView imgitem;
    TextView txtName,txtSmall,txtprice;

    Button cardnearest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);
        getSupportActionBar().hide();

        imgitem=findViewById(R.id.imgitem);
        txtName=findViewById(R.id.txtName);
        txtSmall=findViewById(R.id.txtSmall);
        txtprice=findViewById(R.id.txtprice);
        cardnearest=findViewById(R.id.btnView);

        materials=(Materials) getIntent().getSerializableExtra("data");

        imgitem.setImageResource(materials.getSelected());
        txtName.setText(materials.getData());
        txtprice.setText(materials.getPrice()+getString(R.string.rs));
        txtSmall.setText(materials.getOfferprice()+getString(R.string.rs));

       txtSmall.setPaintFlags(txtSmall.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        cardnearest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(ItemDetailsActivity.this,MapsActivity.class));



            }
        });

    }
}
