package com.centroid.ecommercebuildingmaterials.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.centroid.ecommercebuildingmaterials.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdPageFragment extends Fragment {


    public AdPageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_ad_page, container, false);

        ImageView img=v.findViewById(R.id.img);
        Bundle bundle=getArguments();
        int p=bundle.getInt("key");

        if(p==0)
        {

            img.setImageResource(R.drawable.adone);
        }

        if(p==1)
        {

            img.setImageResource(R.drawable.adtwo);
        }

        if(p==2)
        {

            img.setImageResource(R.drawable.adthree);
        }


        return v;
    }

}
