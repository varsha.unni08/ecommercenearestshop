package com.centroid.ecommercebuildingmaterials.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.centroid.ecommercebuildingmaterials.fragments.AdPageFragment;

public class AdPagerAdapter extends FragmentPagerAdapter {

    public AdPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        Bundle bundle=new Bundle();
        bundle.putInt("key",position);

        Fragment fragment=new AdPageFragment();
        fragment.setArguments(bundle);



        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
