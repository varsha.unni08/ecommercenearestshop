package com.centroid.ecommercebuildingmaterials.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.ecommercebuildingmaterials.R;
import com.centroid.ecommercebuildingmaterials.domain.Materials;
import com.centroid.ecommercebuildingmaterials.views.ItemDetailsActivity;

import java.util.List;

public class MaterialsGridAdapter extends RecyclerView.Adapter<MaterialsGridAdapter.TwoItemHolder> {


    Context context;
    List<Materials>materials;


    public class TwoItemHolder extends RecyclerView.ViewHolder
    {

        ImageView imgitem;
        TextView txtName,txtSmall,txtprice;

        public TwoItemHolder(@NonNull View itemView) {
            super(itemView);

            imgitem=itemView.findViewById(R.id.imgitem);
            txtName=itemView.findViewById(R.id.txtName);
            txtSmall=itemView.findViewById(R.id.txtSmall);
            txtprice=itemView.findViewById(R.id.txtprice);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, ItemDetailsActivity.class);
                    intent.putExtra("data",materials.get(getAdapterPosition()));
                    context.startActivity(intent);

                }
            });



        }
    }


    public MaterialsGridAdapter(Context context, List<Materials> materials) {
        this.context = context;
        this.materials = materials;
    }


    @NonNull
    @Override
    public TwoItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view1= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_oneitemholder,parent,false);

        return new TwoItemHolder(view1);
    }

    @Override
    public void onBindViewHolder(@NonNull TwoItemHolder holder, int position) {

        holder.imgitem.setImageResource(materials.get(position).getSelected());
        holder.txtName.setText(materials.get(position).getData());
        holder.txtSmall.setText(materials.get(position).getPrice()+context.getString(R.string.rs));
        holder.txtprice.setText(materials.get(position).getOfferprice()+context.getString(R.string.rs));

        holder.txtSmall.setPaintFlags( holder.txtSmall.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


    }

    @Override
    public int getItemCount() {
        return materials.size();
    }
}
