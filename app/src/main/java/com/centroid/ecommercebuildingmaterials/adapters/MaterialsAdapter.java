package com.centroid.ecommercebuildingmaterials.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.centroid.ecommercebuildingmaterials.R;
import com.centroid.ecommercebuildingmaterials.domain.Materials;
import com.centroid.ecommercebuildingmaterials.views.ItemDetailsActivity;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MaterialsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


Timer Timer=null;
    Context context;

    public MaterialsAdapter(Context context) {
        this.context = context;
    }

    public static final int ad=0;
    public static final int oneitem=1;

    public static final int twoitem=2;
    public static final int threeitem=3;
    public static final int fouritem=4;




    public class AdHolder extends RecyclerView.ViewHolder{

        ViewPager viewpager;
        TabLayout tabindicator;

        public AdHolder(@NonNull View itemView) {
            super(itemView);
            tabindicator=itemView.findViewById(R.id.tabindicator);
            viewpager=itemView.findViewById(R.id.viewpager);
        }
    }

    public class OnitemHolder extends RecyclerView.ViewHolder{

        TextView txtSmall;

        public OnitemHolder(@NonNull View itemView) {
            super(itemView);

            txtSmall=itemView.findViewById(R.id.txtSmall);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Materials materials1=new Materials();
                    materials1.setData("Premium-Cab FR PVC Insulated 1.0");

                    materials1.setSelected(R.drawable.wire);
                    materials1.setOfferprice("110");
                    materials1.setPrice("115");

                    Intent intent=new Intent(context, ItemDetailsActivity.class);
                    intent.putExtra("data",materials1);
                    context.startActivity(intent);

                }
            });
        }
    }

    public class TwoItemHolder extends RecyclerView.ViewHolder
    {

        RecyclerView recyclerFirst;
        public TwoItemHolder(@NonNull View itemView) {
            super(itemView);

            recyclerFirst=itemView.findViewById(R.id.recyclerFirst);
        }
    }



    public class ThreeitemHolder extends RecyclerView.ViewHolder{

        RecyclerView recyclerthree;
        TextView txtSmall;

        CardView cardMaterial;

        public ThreeitemHolder(@NonNull View itemView) {
            super(itemView);

            recyclerthree=itemView.findViewById(R.id.recyclerthree);
            txtSmall=itemView.findViewById(R.id.txtSmall);
            cardMaterial=itemView.findViewById(R.id.cardMaterial);
        }
    }

    public class FouritemHolder extends RecyclerView.ViewHolder{
        RecyclerView recyclerFirst;

        public FouritemHolder(@NonNull View itemView) {
            super(itemView);
            recyclerFirst=itemView.findViewById(R.id.recyclerFirst);
        }
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        RecyclerView.ViewHolder viewHolder=null;

        switch (viewType)
        {

            case ad:

                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_adholder,parent,false);

                viewHolder=new AdHolder(view);

                break;

            case oneitem:

                View view1= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_oneitemholder,parent,false);

                viewHolder=new OnitemHolder(view1);
                break;

            case twoitem:

                View view2= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_twoitem,parent,false);

                viewHolder=new TwoItemHolder(view2);

                break;

            case threeitem:
                View view3= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_threeitem,parent,false);

                viewHolder=new ThreeitemHolder(view3);

                break;

            case fouritem:

                View view4= LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutfouritem,parent,false);

                viewHolder=new FouritemHolder(view4);

                break;



        }



        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType())
        {


            case ad:

                final AdHolder adHolder=(AdHolder) holder;


                adHolder.viewpager.setAdapter(new AdPagerAdapter(((AppCompatActivity)context).getSupportFragmentManager()));


                for (int i=0;i<3;i++)
                {
                    adHolder.tabindicator.addTab(adHolder.tabindicator.newTab());
                }


                adHolder.tabindicator.setupWithViewPager(adHolder.viewpager);

                adHolder.tabindicator.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {


                        adHolder.viewpager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });



                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        ((AdHolder) holder).viewpager.post(new Runnable(){

                            @Override
                            public void run() {
                                ((AdHolder) holder).viewpager.setCurrentItem((((AdHolder) holder).viewpager.getCurrentItem()+1)%3);
                            }
                        });
                    }
                };
                Timer = new Timer();
                Timer.schedule(timerTask, 3000, 3000);


                break;

            case oneitem:


                final OnitemHolder onitemHolder=(OnitemHolder) holder;



                onitemHolder.txtSmall.setPaintFlags( onitemHolder.txtSmall.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


                break;

            case twoitem:

                final TwoItemHolder twoItemHolder=(TwoItemHolder) holder;

                twoItemHolder.recyclerFirst.setLayoutManager(new LinearLayoutManager(context));

                List<Materials>materials=new ArrayList<>();

                for(int i=0;i<2;i++)
                {
                    Materials materials1=new Materials();

                    if(i==0) {
                        materials1.setData("Premium Quality - White Cement - White Port Land Cement - 400 Grams");

                        materials1.setSelected(R.drawable.whitecement);
                        materials1.setOfferprice("250");
                        materials1.setPrice("330");

                    }

                    if(i==1) {
                        materials1.setData("WEIRD Grade A Premium Quality All Purpose Grey Cement - 400 Grams ");

                        materials1.setSelected(R.drawable.cem);
                        materials1.setOfferprice("250");
                        materials1.setPrice("380");

                    }


                    materials.add(materials1);

                }

                twoItemHolder.recyclerFirst.setLayoutManager(new GridLayoutManager(context,2));

                twoItemHolder.recyclerFirst.setAdapter(new MaterialsGridAdapter(context,materials));

                break;

            case threeitem:

                final ThreeitemHolder threeitemHolder=(ThreeitemHolder) holder;



                List<Materials>materials11=new ArrayList<>();

                for(int i=0;i<2;i++)
                {
                    Materials materials1=new Materials();

                    if(i==0) {
                        materials1.setData("GI Galvanised Binding Wire Used for Construction - 20SWG - 5 kg ");

                        materials1.setSelected(R.drawable.giwire);
                        materials1.setOfferprice("450");
                        materials1.setPrice("530");

                    }

                    if(i==1) {
                        materials1.setData("Bosch 0601B373F0-GCO220 2200-Watt 14-inch Chop Saw Machine (Blue) with GDC 120 Professional Marble Cutter Combo");

                        materials1.setSelected(R.drawable.giii);
                        materials1.setOfferprice("11250");
                        materials1.setPrice("11380");

                    }


                    materials11.add(materials1);

                }

                threeitemHolder.recyclerthree.setLayoutManager(new GridLayoutManager(context,2));

                threeitemHolder.recyclerthree.setAdapter(new MaterialsGridAdapter(context,materials11));

                threeitemHolder.txtSmall.setPaintFlags( threeitemHolder.txtSmall.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


                threeitemHolder.cardMaterial.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Materials materials1=new Materials();
                        materials1.setData("Asian Paints ezyCR8 Royale Health Shield, DIY All Surface Anti Bacterial Clear Coating Paint");

                        materials1.setSelected(R.drawable.paint);
                        materials1.setOfferprice("650");
                        materials1.setPrice("710");

                        Intent intent=new Intent(context, ItemDetailsActivity.class);
                        intent.putExtra("data",materials1);
                        context.startActivity(intent);
                    }
                });



                break;

            case fouritem:



                final FouritemHolder twoItemHolder1=(FouritemHolder) holder;



                List<Materials>materials111=new ArrayList<>();

                for(int i=0;i<4;i++)
                {
                    Materials materials1=new Materials();

                    if(i==0) {
                        materials1.setData("Transparent FRP Roofing Sheet, 5mm");

                        materials1.setSelected(R.drawable.roof);
                        materials1.setOfferprice("250");
                        materials1.setPrice("330");

                    }

                    if(i==1) {
                        materials1.setData("Aluminium Angle Bar Profile Sections for DIY Projects & Other Applications | 38mm Width x 25mm Height x 3mm Thickness | Length - 2ft3inch");

                        materials1.setSelected(R.drawable.aluminium);
                        materials1.setOfferprice("1250");
                        materials1.setPrice("1380");

                    }


                    if(i==2) {
                        materials1.setData("Aluminium Square Tube Box Rectangular ");

                        materials1.setSelected(R.drawable.alusquare);
                        materials1.setOfferprice("250");
                        materials1.setPrice("330");

                    }

                    if(i==3) {
                        materials1.setData("COLOUR COATED PPGL ROOFING SHEETS ");
                        materials1.setSelected(R.drawable.roofingsheet);
                        materials1.setOfferprice("50");
                        materials1.setPrice("80");

                    }


                    materials111.add(materials1);

                }

                twoItemHolder1.recyclerFirst.setLayoutManager(new GridLayoutManager(context,2));

                twoItemHolder1.recyclerFirst.setAdapter(new MaterialsGridAdapter(context,materials111));




                break;




        }





    }

    @Override
    public int getItemCount() {
        return 5;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
