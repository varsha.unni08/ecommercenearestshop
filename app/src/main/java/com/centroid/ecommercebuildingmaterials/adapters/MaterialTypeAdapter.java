package com.centroid.ecommercebuildingmaterials.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.ecommercebuildingmaterials.R;
import com.centroid.ecommercebuildingmaterials.domain.Materials;

import java.util.List;

public class MaterialTypeAdapter extends RecyclerView.Adapter<MaterialTypeAdapter.MaterialtypeHolder>
{


    Context context;
    List<Materials> materialsdata;

    public MaterialTypeAdapter(Context context, List<Materials> arr) {
        this.context = context;
        this.materialsdata = arr;
    }

    public class MaterialtypeHolder extends RecyclerView.ViewHolder{

        TextView txtMaterial;

        CardView card_materials;

        public MaterialtypeHolder(@NonNull View itemView) {
            super(itemView);
            txtMaterial=itemView.findViewById(R.id.txtMaterial);
            card_materials=itemView.findViewById(R.id.card_materials);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {






                }
            });
        }
    }


    @NonNull
    @Override
    public MaterialtypeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_materialtype,parent,false);


        return new MaterialtypeHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MaterialtypeHolder holder, int position) {

        holder.txtMaterial.setText(materialsdata.get(position).getData());
        holder.card_materials.setCardElevation(10);
        holder.card_materials.setRadius(10);

        if (materialsdata.get(position).getSelected() == 1)
        {

            holder.card_materials.setBackgroundColor(Color.parseColor("#ffeec3"));


        }
        else {


            holder.card_materials.setBackgroundColor(Color.parseColor("#ffffff"));



        }

    }

    @Override
    public int getItemCount() {
        return materialsdata.size();
    }
}
